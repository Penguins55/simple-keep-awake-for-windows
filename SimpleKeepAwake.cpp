// SimpleKeepAwake.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
// https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-setthreadexecutionstate
//

#include <iostream>
#include <windows.h>
#include <conio.h>

// ES_CONTINUOUS - Informs the system that the state being set should remain in effect until the next call that uses ES_CONTINUOUS and one of the other state flags is cleared.
//#define ES_CONTINUOUS       0x80000000

// ES_DISPLAY_REQUIRED - Forces the display to be on by resetting the display idle timer.
//#define ES_DISPLAY_REQUIRED 0x00000002

// ES_SYSTEM_REQUIRED - Forces the system to be in the working state by resetting the system idle timer.
//#define ES_SYSTEM_REQUIRED  0x00000001

int main()
{
    int result = 0;
    int count = 0;

    std::cout << "\n\nThis program will reset the idle timer every 30 seconds. \n\n\nPress any key to exit.  \n\n\n";

    while ( _kbhit() == 0 )
    {
        if (count == 0) {
            result = SetThreadExecutionState(ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED | ES_CONTINUOUS);
            std::cout << "\r Timer reset.  ";
        } else 
            std::cout << "\r " << count << "                    ";
        
        //Sleep(30 * 1000);
        Sleep(1000);
        count++;
        if (count > 30)
            count = 0;
    }

    return 0;

}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
